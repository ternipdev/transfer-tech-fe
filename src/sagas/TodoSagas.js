import { takeEvery, put } from 'redux-saga/effects'
import * as types from '../constants'
import uploadDataApi from '../fetchAPIs/uploadAPI'

function* uploadImages(action) {
  try {
    const path = `/api/v1/upload-multi`
    const formdata = new FormData()
    formdata.append('name', action.payload.image)
    const res = yield uploadDataApi('POST', path, formdata)
    console.log(res)
    yield put({
      type: types.UPLOAD_IMAGE_SUCCESS
    })
    console.log('SUCCESS')
  } catch (e) {
    yield put({
      type: types.UPLOAD_IMAGE_FAILURE,
      payload: e
    })
  }
}

export const TodoSaga = [takeEvery(types.UPLOAD_IMAGE_REQUEST, uploadImages)]
