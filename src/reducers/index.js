import { combineReducers } from 'redux';
import TodoReducers from "./TodoReducers";

export default combineReducers({
    todoReducer: TodoReducers
});