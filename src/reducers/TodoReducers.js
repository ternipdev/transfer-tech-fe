import * as types from '../constants'

const DEFAULT_STATE = {
  isLoading: null,
  listData: [],
  err: null,
  errMessage: ''
}

const TodoReducer = (state = DEFAULT_STATE, actions) => {
  switch (actions.type) {
    case types.UPLOAD_IMAGE_REQUEST:
      return {
        ...state,
        isLoading: true
      }
    case types.UPLOAD_IMAGE_FAILURE:
      return {
        ...state,
        isLoading: false,
        err: true,
        errMessage: actions.payload
      }
    case types.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        isLoading: false
      }
    default:
      return {
        state
      }
  }
}

export default TodoReducer
