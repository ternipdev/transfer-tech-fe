import React, { Component } from 'react'
import * as actions from '../actions/TodoActions'
import TodoComponents from '../components/TodoComponents'
import { connect } from 'react-redux'

class TodoContainers extends Component {
  render() {
    return (
      <div>
        <TodoComponents {...this.props} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    uploadImage: (data) => {
      dispatch(actions.uploadData(data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoContainers)
